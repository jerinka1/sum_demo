"""sum demo"""

def sum_two(a_1:int, b_1:int)->int:
    """return sum of two integers"""
    return a_1 + b_1


if __name__=='__main__':
    print(sum_two(1,2))
